# cans microservice

CAN server and record savings.

App params | HTTP<br/>Server | TCP<br/>Server | Former<br/>TCP Server | Serial<br/>Client |
-----------|-----------------|----------------|-----------------------|-------------------|
`cans usb`  | 8081 | 8082 | 2020 | `/dev/ttyUSB0`
`cans spi4` | 8081 | 8082 | 2020 | `/dev/spidev2.0`

## Run
```
$ ./cans
Usage of cans:
  usb
	Run cans using a serial/USB dongle to CAN
  spi4
	Run cans using SPI with four CANs
  client
	Run a simulator to send messages to another can app [usb/spi4]
  version
	Prints the cans version and exit
```
Mode `usb` parameters:
```
$ ./cans usb -h
Usage of usb:
  -debug
    	Prints http API and debug saves
  -http int
    	The HTTP server port (default 8081)
  -p19
    	Connects to parlor19 with TCP port 2020.
  -save string
    	History save path after os.home (default ".cans")
  -serial string
    	The USB device port (default "/dev/ttyUSB0")
  -speed int
    	The USB port bauds (default 115200)
  -tcp int
    	The TCP server port (default 8082)
```
Mode `spi4` parameters:
```
$ ./cans spi4 -h
Usage of cans4:
  -can1
    	CAN/1 active (default true)
  -can2
    	CAN/2 active (default true)
  -can3
    	CAN/3 active (default true)
  -can4
    	CAN/4 active (default true)
  -debug
    	Prints http API and debug saves
  -http int
    	The HTTP server port (default 8081)
  -p19
    	Connects to parlor19 with TCP port 2020.
  -save string
    	History save path after os.home (default ".cans")
  -serial string
    	The SPI device port (default "/dev/spidev2.0")
  -speed int
    	The SPI port speed (default 240000)
  -tcp int
    	The TCP server port (default 8082)
```
Mode `client` parameters:
```
$ ./cans client -h
Usage of client:
  -ip string
    	IP server address (default "127.0.0.1")
  -tcp int
    	TCP client port (default 8082)
```

## HTTP API
Next API is for modes `usb` and `spi4` running.

To use the API use another console, example:
```bash
$ curl -X GET http://192.168.1.71:8081/recs/req | json_pp
```

Nets `/nets/`

 Action | Verb | Path | Details
--------|------|------|--------
 Status | GET  | `/nets` | Health info
 TCP on | POST | `/nets/tcp/on` | Restart TCP

Port `/port/{port}`

 Action | Verb | Path examples | Details
--------|------|------|--------
 Status | GET  | `/port/1` | Health info
  Open  | POST | `/port/1/open` | Reopen port
  
Requests `/can/{can}/req/{req}`

 Action | Verb | Path examples | Details |
--------|------|-------|---------|
 PM Version | POST | `/can/1/req/1,1,0,0`   | Read can/1 pm/1 version number
 PM Serial  | POST | `/can/1/req/1,1,0,1`   | Read can/1 pm/1 serial number
 PM Status  | POST | `/can/1/req/1,1,0,2`   | Read can/1 pm/1 status params
 PM Config  | POST | `/can/1/req/1,1,0,3`   | Read can/1 pm/1 config params
 PM Curves  | POST | `/can/1/req/1,1,0,5,1` | Read can/1 pm/1 curves samples


Records `/recs/{group}/{nllii}/{yymmdd}`

 Action | Verb | Path examples | Details |
--------|------|-------|---------|
Requests     | GET | `/recs/req`              | Get requests Net-Line-Ids list
Req nllii    | GET | `/recs/req/10101`        | Get requests YMDs list
Req yymmdd   | GET | `/recs/req/10101/211217` | Get request YMD binary data
Solicited    | GET | `/recs/sol`              | Get solicited responses
Sol nllii    | GET | `/recs/sol/11001`        |
Sol yymmdd   | GET | `/recs/sol/11001/211217` |
Unsolicited  | GET | `/recs/unsol`            | Get unsolicited responses
Unsol nllii  | GET | `/recs/unsol/28001`
Unsol yymmdd | GET | `/recs/unsol/28001/211229`



