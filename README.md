# CANs documentation

## Parlor 2019 version
```
                 ............................................................
,-----------,    : http  ,----------,   http ,----------,   tcp   ,---------:-----,
|  GET/POST |----:-(1)-->|8080      |--(2)-->|8888      |<<<(3)>>>|2020     :     | cans
| websocket |<<<<<<(1ws)<|          |<<(2ws)<|          |         |         :     |
|   200/4xx |<- -:- - - -|   Node   |<- - - -|  Java    |         | C app   :SPI  |<--> 
'-----------'    :       |   app    |        |  app     |         |         :chips|<--> Equipment
  Browser        :  SOM  |      8080|<--(4)--|  parlor  |         |         :     |<-->
  (one of many)  :       |          |- - - ->|  AUTH    |         |         :     |<--> 
                 :       | reverse  | http   '----------'         '---------:-----'
                 :       | proxy &  |                                       : /spidev2.0
                 :       | history  |   http ,----------,                   :
                 :       |          |--(5)-->| 27017    |                   :
                 :       |          |<- - - -| Mongo    |                   :
                 :       '----------'        '----------'                   :
                 :..........................................................:
```
Messages

| Origin | Action | Route
|--------|--------|---------
| Browser | Get Realtime Pages  | `(1)->(2)`
| Browser | Request Equipment   | `(1)->(2)->(3)`
| Equipment | Event update      | `(3)->(2ws)->(1ws)`
| Equipment | Event save        | `(3)->(4)->(5)`
| Browser | Get History Reports | `(1)->(5)`

Microservices

|      | Reverse<br/>proxy | Realtime,<br/>History | CANs
|------|-------------------|----------------------|-----|
| Ports       | 8080     | 8888        | 2020
| Websockets  | redirect | serve       | --
| HTML Access | redirect | Login, Role | --
| Records     | report   | parse, save | --
| I18N        | (yes)    | yes         | --
| Timezones   | yes      | no          | --
| Programming | Node     | Java        | C 

## Parlor 2022 version
```
                 ............................................................
,-----------,    : http  ,---------,  http  ,-----------,   http  ,---------:-----,
|  GET/POST |----:-(1)-->|8080     |--(2)-->|8081       |---(3)-->|8082     :     | cans
| websocket |<<<<:<(1ws)<|         |<<(2ws)<|           |         |         :     | 
|   200/4xx |<- -:- - - -|         |<-------|  rt-cans  |<--------|         :SPI  |<--> 
'-----------'    :       |  ??     |        |  Go app   |         |  cans   :FTDI |<--> Equipment
  Browser        :  SOM  |  app    |        |  ROLES    |  tcp    |  Go app :chips|<-->
  (one of many)  :       |         |        |  I18N     |<-(4)----|8083     :     |<--> 
                 :       | reverse |        '-----------'         |         :-----' 
                 :       | proxy   |                              |         : /spidev2.0
                 :       |         |       2019 version  <<<(x)>>>|2020     : /ttyUSBx
                 :       | AUTH    |                              |         :
                 :       |         |        ,-----------,         '-(5)-(6)-:
                 :       |         |--(7)-->|8084       |            |   |  :
                 :       |         |<- - - -|  db-cans  |            v   ^  :
                 :       |         |        |  ?? app   |--(8)-> Filesystem :  
                 :       |         |        |  history  |<------   Database :
                 :       '---------'        '-----------'              ^    :
 Cloud / VNP     :                                                     |    :
   History    <--:-(9)--> rsync / scp <-------------(10)---------------'    :
   Clients       :..........................................................:
```
Messages

| Origin | Action | Route
|--------|--------|---------
| Browser   | Realtime Pages        | `(1)->(2)`
| Browser   | Request Equipment     | `(1)->(2)->(3)`
| Equipment | Event update        | `(4)->(2ws)->(1ws)`
| Equipment | Event save          | `(5)`
| Browser   | History Reports       | `(1)->(7)->(8)`
| VPN       | History Backup/Delete | `(9)->(10)->(9)`

Microservices

|  | Reverse<br/>proxy | History | Realtime | CANs
|--|-------------------|---------|----------|-----
| Ports       | 8080   | 8084          | 8081    | 8082, 8083, 2020
| Websockets  | bypass | no            | serve   | no
| HTTP Auth   | Login  | By role       | By role | --
| Records     | --     | report, parse | parse   | save, read
| I18N        | yes    | yes           | yes     | --
| Timezones   | --     | yes           | --      | --
| Programming | ??     | ??            | Go      | Go

* [cans](cans.md)
* [rt-cans](rt-cans.md)
* db-cans


