# Realtime CANs Parlor pages

CAN realtime Parlor pages.

| App modes | HTTP API | Details
|-----------|----------|--------
| `rt-cans reals ...` | Realtime viewer/config which communicates with cans app.
| `rt-cans demo ...`  | Reals + sim application
| `rt-cans sim ...`   | Replaces cans app with browser simulations.
| `rt-cans version`   | Prints app version

## Modes reals/demo
Mode `reals` is the final realtime parlor application which communicates with `cans` app.
Mode `demo` runs a reals version plus simulations to replace `cans` app with web pages.

Parameters for both `$ ./rt-cans reals -h` and `$ ./rt-cans demo -h`:
```
Usage:
  rt-cans reals [flags]

Flags:
  -H, --HTTP int       Parlor microservice HTTP port.
                       Reverse proxy use this port to redirect browsers.
                       Client's GET/POST reach Parlor pages to navigate/command.
                        (default 8080)
  -z, --zones string   Parlor stalls within zone(s). 
                       * Rotary: 
                         Set a single zone with a single integer for stalls.
                         Example: -z40 for forty stalls.
                       * Parallel/Herringbone: 
                         Set two zones with two ints for stalls separated by a comma.
                         Example: -z20,20 for Double-twenty, forty stalls.
  -o, --order string   Parlor stalls order (default 0,0).
                       * Rotary: 
                         Use two integers Angle [0-360] and Rotation: [0-1].
                         - Angle is where stall number 1 is located, examples:
                             0 = stall 1 at north. Is default.
                            90 = stall 1 at east(cw) or west(ccw).
                           180 = stall 1 at south.
                         - Rotation:
                           0 = clockwise. Is default.
                           1 = counter-clockwise
                         Example: -z40 -o90,1
                       * Parallel/Herringbone: 
                         Use three integers Tilt [0-1], Layout [0-7], Entrance [0,3];
                         - Tilt:
                           0 = parallel. Is default
                           1 = herringbone.
                         - Layout:
                                 zone 1         zone 2
                              -------------  -------------
                              stalls  view   stalls  view
                              ------ ------  ------ ------
                           0 = incr  top      incr  bottom. Is default
                           1 = decr  top      incr  bottom
                           2 = incr  top      decr  bottom
                           3 = decr  top      decr  bottom
                           4 = incr  bottom   incr  top
                           5 = decr  bottom   incr  top
                           6 = incr  bottom   decr  top
                           7 = decr  bottom   decr  top
                           Example: -z20,20 -o1,7
                         - Entrance:
                                           view
                                         ---------
                           0 = At left   Landscape is default.
                           1 = At top    Portrait
                           2 = At right  Landscape
                           3 = At bottom Portrait
                           
  -I, --ins string     Parlor inputs (default 0,0,0).
                       * Rotary:
                         Use one integer Wash [0-1].
                         - Wash is for washing zones, examples:
                           0 = no washing.
                           1 = one washing at zone 0.
                         Example: -z120 -r1 -i1 -I1
                       * Paralell:
                         Use three integers Wash[0-1], Entrance[0-1], Exit[0-1]
                         - Wash is for washing zones, examples:
                           0 = no washings.
                           1 = two washings at zones 1 and 2.
                         - Entrance is for entrance gate zones, examples:
                           0 = no entrance gates.
                           1 = two entrance gates at zones 1 and 2.
                         - Exit is for exit gates existence, examples:
                           0 = no exit gates.
                           1 = two exit gates at zones 1 and 2.
                         Example: -z40,40 -i1 -I1,1,1
                       
      --cIP string     CANs microservice IP address.
                       This realtime clients use this address to connect to CANs for both HTTP/TCP.
                       Use localhost or 127.0.0.1 (default) when CANs microservice runs in the 
                       same machine realtime is running.
                        (default "localhost")
      --cHTTP int      CANs microservice HTTP port.
                       This realtime HTTP client uses this port to access CANs API.
                        (default 8081)
      --cTCP int       CANs microservice TCP port.
                       This realtime TCP client uses this port to receive CANs responses.
                        (default 8082)
  -c, --cans int       Number of nets in cans microservice.
                       Realtime will use the total number of networks adding cans and sims.
                       * Example: -c4.
  -s, --sims int       Number of nets for HTML simulations.
                       Realtime will use the total number of networks adding cans and sims.
                       * Example: -s2.
  -p, --p1s string     Number of Pulsator boxes in nets.
                       List of nets indicating how many PM boxes are in each net.
                       * Example: -c2 -p0,40 
                         means no PMs in net #1 and forty PMs in net #2.
  -t, --t4s string     Number of Takeoff-4 boxes in nets.
                       List of nets indicating how many T4 boxes are in each net.
                       * Example: -c2 -t0,5 
                         means no T4s are in net #1 but five T4s (20 takeoffs) are in net #2.
  -i, --i4s string     Number of Input-4 boxes in nets.
                       List of nets indicating how many I4 boxes are in each net.
                       * Example: -c2 -i0,1 
                         means one I4 box is in net #2.
  -r, --rss string     Number of Rotary Stalls Identifiers in nets.
                       List of nets indicating how many RS boxes are in each net.
                       * Example: -r1 
                         means one RS box in in net #1.
                       
  -h, --help           help for reals
```

### Both modes `reals` and `demo` HTML API:

 View | Verb | Path | Details
------|------|------|--------
Default | GET  | `/parlor` | Parlor pages to see events and config equipment.
Boxes   | GET  | `/parlor/boxes` | Pages to discover serials equipment.
Dash    | GET  | `/parlor/dash`  | Dashboard (future)
History | GET  | `/parlor/history` | Simple history (future)

### Mode `demo` only HTML API:

 View | Verb | Path | Simulations
------|------|------|------------
Simulator attrs | GET | `/parlor/sim/attr/reals` | Parlor DOM objects
Simulator reals | GET | `/parlor/sim/cans/reals` | Equipment config/states
Simulator boxes | GET | `/parlor/sim/cans/boxes` | Equipment serials 

## Mode sim
`sim` mode is part of the demo as an apart app in order to interact with `reals` mode
app replacing the simulation a `cans` app.

Parameters for `$ ./rt-cans sim -h`

```
Usage:
  rt-cans sim [flags]

Flags:
      --sHTTP int    Simulator CANs microservice HTTP port.
                     The realtime HTTP client must match this port to access CANs simulator API.
                      (default 8081)
      --sTCP int     Simulator CANs microservice TCP port.
                     The realtime TCP client must match this port to receive CANs simulator responses.
                      (default 8082)
  -c, --cans int     Number of nets in cans microservice.
                     Realtime will use the total number of networks adding cans and sims.
                     * Example: -c4.
  -s, --sims int     Number of nets for HTML simulations.
                     Realtime will use the total number of networks adding cans and sims.
                     * Example: -s2.
  -p, --p1s string   Number of Pulsator boxes in nets.
                     List of nets indicating how many PM boxes are in each net.
                     * Example: -c2 -p0,40 
                       means no PMs in net #1 and forty PMs in net #2.
  -t, --t4s string   Number of Takeoff-4 boxes in nets.
                     List of nets indicating how many T4 boxes are in each net.
                     * Example: -c2 -t0,5 
                       means no T4s are in net #1 but five T4s (20 takeoffs) are in net #2.
  -i, --i4s string   Number of Input-4 boxes in nets.
                     List of nets indicating how many I4 boxes are in each net.
                     * Example: -c2 -i0,1 
                       means one I4 box is in net #2.
  -r, --rss string   Number of Rotary Stalls Identifiers in nets.
                     List of nets indicating how many RS boxes are in each net.
                     * Example: -r1 
                       means one RS box in in net #1.
                     
  -h, --help         help for sim
```
### Mode `sim` only HTML API:

Equipment config/state HTTP API. Query params not shown.

| Verb | `{r}` = `/parlor/sim/cans/reals`
|------|------
| GET  | `{r}/`
| GET  | `{r}/ws`
| POST | `{r}/{can}/p1/{id}/power/{power}`
| POST | `{r}/{can}/p1/{id}/status/{front}/{rear}`
| POST | `{r}/{can}/p1/{id}/notify/{type}/{value}`
| POST | `{r}/{can}/p1/{id}/values/{side}`
| POST | `{r}/{can}/p1/{id}/resp/{cfg}`
| POST | `{r}/{can}/p1/{id}/save/{cfg}`
| POST | `{r}/{can}/rs/{id}/power/{power}`
| POST | `{r}/{can}/rs/{id}/stall/{gate}/{reader}`
| POST | `{r}/{can}/t4/{id}/power/{power}`
| POST | `{r}/{can}/t4/{id}/button/{btn:[1-4]}/status`
| POST | `{r}/{can}/t4/{id}/button/{btn:[1-4]}/mode/{mode}`
| POST | `{r}/{can}/t4/{id}/button/{btn:[1-4]}/level/{ohms}`
| POST | `{r}/{can}/t4/{id}/button/{btn:[1-4]}/leds/{blink}/{color}`
| POST | `{r}/{can}/i4/{id}/power/{power}`
| POST | `{r}/{can}/i4/{id}/change/{type}/{zone}/{logic}/{value}`

Equipment serials HTTP API

| Verb | `{r}` = `/parlor/sim/cans/boxes`
|------|------
| GET  | `{r}/`
| GET  | `{r}/ws`
| GET  | `{r}/{can}/{serial}/get/{line}/{id}`
| GET  | `{r}/{can}/{serial}/version/{line}/{id}/{version}`

## Operation `./rt-cans reals` with `./cans dongle`

* Run cans in dongle mode (dongle at ttyUSB0) in a first console:
```bash
$ sudo ./cans usb -debug
```

* Run rt-cans in real mode in a second console:
```bash
$ ./rt-cans reals -c1 -p40 -z40
```
* Open browser at http://localhost:8081/parlor

```
,-----------,    : http  ,-----------,   http  ,---------:-----,
|  GET/POST |----:-(1)-->|8081       |---(3)-->|8082     :     | cans
| websocket |<<<<:<(1ws)<|           |         |         :     | 
|   200/4xx |<- -:- - - -|  rt-cans  |<--------|         :     |     
'-----------'    :       |  Go app   |         |  cans   :FTDI |<--> Equipment
  Browser        :  SOM  |  ROLES    |  tcp    |  Go app :chips|    
                 :       |  I18N     |<-(4)----|8083     :     |     
                 :       '-----------'         |         :-----' 
                 :                             |         :           
                 :                             |         : /ttyUSBx
                 :                             |         :
                 :                             '-(5)-(6)-:
```








